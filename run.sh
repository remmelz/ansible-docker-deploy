#!/bin/bash

playbook="build.yml"

##########################################################
# Arguments
##########################################################

# Define list of arguments expected in the input
optstring="dbpg:r:d:e:i:"

restore=""
backup=""
deploy=""
prepare=""
inventory=""
extra_vars=""
encpass=""
gitbranch=""
gitcheckout=""

while getopts ${optstring} arg; do
  case "${arg}" in

    d) deploy="true"          ;;
    p) prepare="true"         ;;
    b) backup="true"          ;;
    r) restore=${OPTARG}      ;;
    i) inventory=${OPTARG}    ;;
    e) encpass="${OPTARG}"    ;;
    g) gitbranch="${OPTARG}"  ;;

    ?)
      echo "Invalid option: -${OPTARG}."
      echo
      echo "Usage: $0 -i <inventory_file> <options>"
      echo
      echo "Options:"
      echo
      echo "  -i <inventory>        build and deploy apps on target host"
      echo "  -b                    backup all docker volumes on target host"
      echo "  -r <backup_folder>    restore backup of docker volumes on target host"
      echo "  -e <encryption_pass>  encryption password to be used for backup"
      echo "  -g <git_branch>       switch roles to the git branch"
      echo
      exit
      ;;
  esac
done

##########################################################
# Checking input
##########################################################

if [[ -z ${inventory} ]]; then
  echo "Error: not a valid inventory target"
  echo
  echo "Usage: $0 -i <inventory_file> <options>"
  echo
  echo "Choose one below:"
  find ./inventory -name *.ini
  echo
  exit 1
fi

if [[ -z `cat ${inventory} | grep ansible_` ]]; then
  echo "Error: not a valid inventory filename"
  exit 1
fi

inventory_dir=`dirname ${inventory}`
if [[ ! -f "${inventory_dir}/config.yml" ]]; then
  echo "Error: no config.yml file found."
  exit 1
fi

if [[ -z `ls -1 ${inventory_dir}/apps/ | grep 'yml$'` ]]; then
  echo 'Error: no app services files found.'
  exit 1
fi

##########################################################
# Set GIT branch to ...
##########################################################

if [[ -n `echo ${gitbranch} | grep 'checkout-'` ]]; then
  gitcheckout=yes
  gitbranch=`echo ${gitbranch} | awk -F'-' '{print $2}'`
  if [[ -n `echo ${gitbranch} | grep default` ]]; then
    gitbranch=""
  fi
fi

if [[ -z ${gitbranch} ]]; then
  gitbranch=`grep '^git_branch' \
    ${inventory_dir}/config.yml \
    | awk -F '"' '{ print $2 }'`

  if [[ -z ${gitbranch} ]]; then
    git_branch="master"
  fi
fi

echo "Using git branch ${gitbranch}"; echo
cd ./roles
for role in `ls -1`; do
  if [[ -d ./${role}/.git ]]; then
    cd ./${role}
    git checkout ${gitbranch} > /dev/null 2>&1
    cd ../
  fi
done
cd ../

[[ -n ${gitcheckout} ]] && exit

##########################################################
# Start playbook
##########################################################

if [[ -n ${backup} ]]; then

  echo "***********************"
  echo "* Creating Backup     *"
  echo "***********************"
  
  playbook="backup.yml"
fi

if [[ -n ${restore} ]]; then

  if [[ -z ${encpass} ]]; then
    read -sp 'Password: ' encpass
    echo
  fi

  echo "***********************"
  echo "*  Restoring Backup   *"
  echo "***********************"
 
  playbook="restore.yml"
  extra_vars="restore_date=${restore} backup_encpass=${encpass}"
fi

if [[ ${playbook} == "build.yml" ]]; then

  echo "***********************"
  echo "* Build and deploy    *"
  echo "***********************"
fi

sleep 3

ansible-playbook -v \
    -e "${extra_vars}" \
    -i ${inventory} \
    ./playbooks/${playbook}

exit


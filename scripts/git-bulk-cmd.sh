#!/bin/bash

cmd=$1
arg=$2

#set -x

function git_pull()
{
  git checkout master
  git pull
}

function git_checkout()
{
  git checkout $1    
  echo
}

function git_status()
{
  c=0
  while [[ $c -lt 60 ]]; do
    printf '-'
    let c=$c+1
  done
  printf ' '
  git status
  echo;echo
}

function git_commit()
{
  [[ -n `git status | grep -i 'nothing to commit'` ]] && return
  git add .
  git commit -m $2
  git push
}

cd ./roles || exit 1

for role in `ls`; do

  echo ${role}
  cd ${role}

  case ${cmd} in
    status)
      git_status ${role} ;;
    commit)
      git_commit ${role} ${arg};;
    checkout)
      git_checkout ${arg};;
    pull)
      git_pull;;
  esac

  cd ..
done


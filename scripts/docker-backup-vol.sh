#!/bin/bash

###########################################
# Default vars
###########################################

vol_path="/var/lib/docker/volumes"

###########################################
# Arguments
###########################################

# Define list of arguments expected in the input
optstring="R:k:r:s:b:f"

force=0
restore=""

while getopts ${optstring} arg; do
  case "${arg}" in

    R) restore="${OPTARG}"    ;;
    k) key_file="${OPTARG}"   ;;
    r) retention="${OPTARG}"  ;;
    b) bak_path=${OPTARG}     ;;
    s) syncthing=${OPTARG}    ;;
    f) force=1                ;;

    ?)
      echo "Invalid option: -${OPTARG}."
      echo
      usage
      ;;
  esac
done

###########################################
# Sanity check
###########################################

bak_date=`date '+%Y%m%d'`

if [[ -z `whoami | grep root` ]]; then
  echo "Error: Needs to run as root."
  exit 1
fi

if [[ `docker ps | wc -l` -gt 1 ]]; then
  echo "Error: One or more containers are still running."
  exit 1
fi

if [[ -z ${key_file} ]]; then
  echo "Error: No keyfile for encryption found."
  exit 1
fi

cd ${vol_path} || exit 1

###########################################
# Restoring backups
###########################################

if [[ -n ${restore} ]]; then

  echo "-> Restore volumes.."

  cd ${restore} || exit 1
  
  for enc_archive in `ls -1`; do

    archive=`echo ${enc_archive} | sed 's/.gpg//g'`

    gpg --batch --decrypt           \
      --passphrase-file ${key_file} \
      ${enc_archive} > ${archive}   \
      && rm ${enc_archive}
  done

  ###########################################
  # Restoring volumes
  ###########################################

  for docker_vol in `ls -1 *.tar.gz`; do
  
    vol_name=`echo ${docker_vol} | awk -F'.' '{print $1}'`
    
    if [[ -z `echo ${vol_name} | egrep '(.*?)_(.*?)_(.*?)'` ]]; then
      continue
    fi

    if [[ -d ${vol_path}/${vol_name} ]]; then
      if [[ ${force} -gt 0 ]]; then
        rm -rf ${vol_path}/${vol_name}
      else
        echo "Error: volume ${vol_name} already exists."
        contine
      fi
    fi
    
    echo "Unpacking ${vol_name}..."
    tar xzf ${vol_name}.tar.gz -C ${vol_path}/

  done

  exit
fi

###########################################
# Creating new backup date folder
###########################################

if [[ -d ${bak_path}/${bak_date} ]]; then
  rm -v ${bak_path}/${bak_date}/*
else
  mkdir -vp ${bak_path}/${bak_date}
fi

###########################################
# Backup each volume
###########################################

echo "-> Backup volumes.."
for container in `docker ps \
  -a --format '{{.Names}}'`; do

  for volume in `docker inspect ${container} \
    | egrep 'Source(.*?)docker/volume'       \
    | awk -F'"' '{print $4}'                 \
    | sed 's|/_data||g'                      \
    | sort -u`; do

    volume=`basename ${volume}`

    # Skip prometheus volume which is not important.
    [[ -n `echo ${volume} | grep 'prometheus_data'` ]] && continue

    tar cf ${volume}.tar ./${volume}
    gzip -v ${volume}.tar
    mv -v ${volume}.tar.gz ${bak_path}/${bak_date}/

  done
done

###########################################
# Encrypt files
###########################################

cd ${bak_path}/${bak_date}

for archive in `ls -1`; do
  gpg --batch                     \
    --symmetric                   \
    --cipher-algo AES256          \
    --passphrase-file ${key_file} \
    ${archive} && rm ${archive}
done
    
###########################################
# Syncthing Sync folder
###########################################

if [[ -z `echo ${syncthing} | egrep -i 'none|false'` ]]; then
  if [[ ! -d ${syncthing} ]]; then
    echo "Error: could not find sync folder: ${syncthing}"
  else
    echo "-> Copy files to Syncthing folder.."
    rm -f ${syncthing}/*.tar.gz*
    cp -v ${bak_path}/${bak_date}/* ${syncthing}/
  fi
fi

###########################################
# Cleanup old backups
###########################################

echo "-> Cleanup old backups.."
cd ${bak_path} || exit 1
find . -type f -name *.tar.gz     -mtime +${retention} | xargs rm -fv
find . -type f -name *.tar.gz.gpg -mtime +${retention} | xargs rm -fv
rmdir * 2> /dev/null

###########################################
# Display summary
###########################################

echo
echo "--- Summary ----------"
ls -lh ${bak_path}/${bak_date}

###########################################

exit 0

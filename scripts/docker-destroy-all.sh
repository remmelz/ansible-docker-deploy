#!/bin/bash

read -p "Are you sure? (y/N): " YESNO
[[ -z `echo ${YESNO} | grep -i y` ]] && exit 1

#############################################

if [[ `docker ps | wc -l` -gt 1 ]]; then
  echo "Error: One or more containers are still running."
  exit 1
fi

#############################################

echo "-> Removing containers.."
for container in `docker ps \
  -a --format '{{.Names}}'`; do

  docker rm ${container} 
done

#############################################

c=0
force=""
echo "-> Removing images.."
while [[ c -le 3 ]]; do
  for image in `docker images -a \
    | awk -F' ' '{print $3}' \
    | grep -v IMAGE`; do
  
    [[ $c -eq 3 ]] && force="--force"
    docker rmi ${force} ${image} 
  done
  let c=$c+1
done

#############################################

echo "-> Removing volumes.."
for volume in `docker volume ls \
  | awk -F' ' '{print $2}' \
  | grep -v VOLUME`; do

  docker volume rm ${volume} 
done
 


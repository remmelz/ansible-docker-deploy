#!/bin/bash

echo "-> Starting backend containers.."
for container in `docker ps \
  -a --format '{{.Names}}' | egrep 'mariadb|postgres'`; do

  docker start ${container} 
done

echo "-> Starting frontend containers.."
for container in `docker ps \
  -a --format '{{.Names}}' \
  | egrep -v 'mariadb|postgres|proxy'`; do

  docker start ${container} 
done

echo "-> Starting nginx container.."
for container in `docker ps \
  -a --format '{{.Names}}'| grep proxy`; do

  docker start ${container} 
done



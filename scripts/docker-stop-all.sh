#!/bin/bash

echo "-> Stopping containers.."
for container in `docker ps \
  -a --format '{{.Names}}'`; do

  docker stop ${container} 
done

sleep 5
if [[ `docker ps | wc -l` -gt 1 ]]; then
  echo "Error: One or more containers are still running."
  exit 1
fi


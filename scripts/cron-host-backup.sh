#!/bin/bash

if [[ -z $1 ]]; then
  echo "Error: no target host(s) given."
  exit 1
fi

script=`basename $0`
scriptdir=`echo $0 | sed "s/${script}$//g"`

cd ${scriptdir} || exit 1
cd ..

for target in $@; do

  # Execute backup procedure
  ./run.sh -i ./inventory/${target}/inventory.ini -b

done


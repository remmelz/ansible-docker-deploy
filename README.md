# Ansible Docker Deploy

This project contains a set of Ansible playbooks which allows you to deploy
docker containers to a server. The inventory folder contains an example folder
for running several services like Matrix, GravCMS or DokuWiki etc..The playbook
automaticly downloads the required Ansible Role and will deploy the selected
services. It also handles SSL certificates from Lets-Encrypt to ensure a secure
HTTPS connection.

![diagram1](images/diagram1.png "Diagram")

## Requirement Software

The following software is required to run it:

  * Git
  * Ansible
  * Debian/Archlinux target host with root privileges
  * SSH connection to the target host
  * Domain + Subdomains points to the target host


## Getting started

In this example i will show you how to deploy a simple website on a server
called server1. Go to the inventory folder and copy the example directory.

    cp -R ./inventory/example ./inventory/server1

Open the config.yml file to change the default settings:

    vi ./inventory/server1/config.yml

The configuration file is self explanatory. Change the following settings:

  * Contact information to allow Lets-encrypt send email when a certificate is about to expire.
  * Turn of the dummy certs called "snakeoil_certs" to False in case you wat Letsencrypt SSL certs.
  * Configure the backup settings and set a proper password.


## Configure ansible inventory

Edit the host.ini file. This file defines the way which user Ansible should use
to login to the remote server. You need to check if Ansible can succesfully
connect to the target host:

    vi ./inventory/server1/host.ini

You can now check if Ansible can succesfully connect to the target host:

    ansible all -i inventory/server1/host.ini -m ping
    server1 | SUCCESS => {
        "changed": false,
        "ping": "pong"
    }


## Configure your first service

Open the default Grav CMS container module file. Change the container stack
and domain name accordingly.

    vi ./inventory/server1/apps/gravcms.yml


## Other services (apps)

To get a list of the available services:

    ls ./inventory/server1/apps/available

For example enable and configure matrix deployment:

    cp -v ./inventory/server1/apps/available/matrix.yml ./inventory/server1/apps/
    vi ./inventory/server1/apps/matrix.yml


## Start deploying

Execute the run script to start the deployment.

    ./run -i ./inventory/server1/host.ini


## Creating backups

To run the backup playbook, you just add the argument -b to the command:

    ./run -i ./inventory/server1/host.ini -b

The backup process includes:

  * Shutting down all of the running containers
  * Archive each persistant docker volume using the tar and gzip command
  * Encrypt each archive volume by using gpg encryption
  * The archive volumes are placed in the backup folder defined in the config.yml
  * If defined, a copy of the backup is placed in the Syncthing folder in order to copy it to other hosts
  * If defined, the archive volumes are retrieved to the backup folder of the Ansible playbook location
  * The containers are started again onces the backup process is finished


## Restoring the backups

To restore all of the persistant docker volumes, you need to use the -r option followed by the location of the docker backup volumes. Enter the backup password to decrypt the volumes:

    ./run -i ./inventory/server1/host.ini -r ./backups/server1/20210818/
    Password: *******

Now that all of the persistant volumes are in place, you need to rerun the deployment playbook:

    ./run -i ./inventory/server1/host.ini

After finished the playbook, the services should now be available again.






